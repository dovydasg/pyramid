﻿using System;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace ConsoleApp1
{
    public static class Helpers
    {
        /// <summary>
        /// This methods tries to extract number from given row/line. 
        /// If it contains other symbols then number or space it will fail to parse and return a false value.
        /// </summary>
        public static bool TryExtractNumber(this string row, out int[] array)
        {
            var correctNumbersCount = SumOfFactorial(row.Split('\n').Length);
            row = Regex.Replace(row, "[^0-9] |\r\n?|\n", " ");

            //fastest way to check string, regex is pretty slow
            if (!row.All(c => Char.IsDigit(c) || c.Equals(' ')))
            {
                Console.WriteLine("Pyramid contains non numerical values");

                array = null;
                return false;
            }

            array = Regex
                    .Matches(row, "[0-9]+")
                    .Cast<Match>()
                    .Select(m => int.Parse(m.Value)).ToArray();

            if (array.Length != correctNumbersCount)
            {
                Console.WriteLine("Pyramid structure is bad");

                array = null;
                return false;
            }

            return true;
        }

        public static bool IsEven(this int value) => value % 2 == 0;

        /// <summary>
        /// Reads a file content and returns as a string
        /// </summary>
        public static string ReadFromFile(string filename)
        {
            Stream fileStream = null;

            try
            {
                fileStream = File.OpenRead(filename);

                using (var streamReader = new StreamReader(fileStream))
                {
                    return streamReader.ReadToEnd();
                }
            }
            catch
            {
                Console.WriteLine("File does not exists");
                return null;
            }
            finally
            {
                if (fileStream != null)
                {
                    fileStream.Close();
                }
            }
        }

        /// <summary>
        /// Returns count of valid pyramid depending on rows count
        /// </summary>
        private static int SumOfFactorial(int rowCount)
        {
            if (rowCount <= 1)
                return 1;
            return rowCount + SumOfFactorial(rowCount - 1);
        }
    }
}
