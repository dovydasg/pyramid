﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace ConsoleApp1
{
    class Node
    {
        public int Value { get; set; }
        public Node Left { get; set; }
        public Node Right { get; set; }

        public Node(int[] values) : this(values, 0, 1) { }

        Node(int[] values, int index, int row)
        {
            if(values != null && values.Length > 0)
                Initialize(values, index, row);
        }

        void Initialize(int[] values, int index, int row)
        {            
            this.Value = values[index];
            
            if(index + row < values.Length)
            {
                this.Left = new Node(values, index + row, row + 1);

                this.Right = new Node(values, index + row + 1, row + 1);
            }
            
        }

        //just for testing porposes
        public override string ToString()
        {
            return string.Format("Node value: {0}, left node value: {1}, right node value: {2}"
                                    , this.Value.ToString()
                                    , this.Left != null ? this.Left.Value.ToString() : "null"
                                    , this.Right != null ? this.Right.Value.ToString() : "null");
        }
    }

    class Program
    {
        private static string assemblyPath = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location).ToString();
        private const string fileName = "/input.txt";

        static void Main(string[] args)
        {
            Node mainNode = new Node(GetInput());
            var maxPath = SolvePyramid(mainNode);

            Console.WriteLine($"Path: {string.Join(",", maxPath)}");
            Console.WriteLine($"Sum of path: {maxPath.Sum()}");
            
            Console.ReadLine();
        }


        #region private methods
        private static void IterateThroughPyramid(Node node, bool isEven, List<int> path, List<List<int>> results)
        {
            if (node.Left == null)
            {
                results.Add(path);
            }
            else
            {
                if (isEven != node.Left.Value.IsEven())
                {
                    List<int> pathLeft = new List<int>(path);
                    pathLeft.Add(node.Left.Value);
                    //if number was even then passing oposite state and visa versa
                    IterateThroughPyramid(node.Left, !isEven, pathLeft, results);
                }
                if (isEven != node.Right.Value.IsEven())
                {
                    List<int> pathRight = new List<int>(path);
                    pathRight.Add(node.Right.Value);
                    //if number was even then passing oposite state and visa versa
                    IterateThroughPyramid(node.Right, !isEven, pathRight, results);
                }
            }
        }

        private static int[] GetInput()
        {
            const string input = @" 1
                                   8 9
                                  1 5 9
                                 4 5 2 3";

            /* uncomment if wanna get data from file
             * var inputFromFile = Helpers.ReadFromFile(assemblyPath + fileName);
            */

            int[] integersArray;

            if (input.TryExtractNumber(out integersArray))
            {
                return integersArray;
            }

            return null;
        }

        private static List<int> SolvePyramid(Node root)
        {
            //should include top node from the start
            List<int> path = new List<int> { root.Value };

            var result = new List<List<int>>();

            //depending on first number if its even
            IterateThroughPyramid(root, root.Value.IsEven(), path, result);

            var maxPath = result.OrderByDescending(list => list.Sum()).First();

            return maxPath;
        }
        #endregion
    }

}
